import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String url = 'http://localhost:8080/get';

  Future<String> makeRequest() async {
    var response = await http.post(Uri.encodeFull(url),
        body: json.encode({
          "NewHourMeterReading": "650",
          "EquipmentNumber": "34665",
          "NewFuelMeterReading": "650"
        }),
        headers: {
          "content-type": "application/json",
          "accept": "application/json"
        });

    print(response.body);
    return response.body;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Center(
            child: new RaisedButton(
      child: new Text('Make Request'),
      onPressed: makeRequest,
    )));
  }
}
