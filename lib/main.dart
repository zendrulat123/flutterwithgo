import 'package:flutter/material.dart';
import 'package:myapp/Login.dart';

import './firstscreen.dart';
import './secondscreen.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    // Start the app with the "/" named route. In this case, the app starts
    // on the FirstScreen widget.
    initialRoute: '/',
    routes: {
      // When navigating to the "/" route, build the FirstScreen widget.
      '/first': (context) => FirstScreen(),
      // When navigating to the "/second" route, build the SecondScreen widget.
      '/': (context) => SecondScreen(),
      '/login': (context) => Loginscreen(),
    },
  ));
}
