import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

//void main() => runApp(MyApp());

class SecondScreen extends StatelessWidget {
  final List<Login> login;

  SecondScreen({Key key, this.login}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Daily Login App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Quote of the Day'),
        ),
        body: Center(
          child: FutureBuilder<List<Login>>(
            future: fetchPhotos(http.Client()),
            builder: (context, snapshot) {
              if (snapshot.hasError) print(snapshot.error);

              return snapshot.hasData
                  ? LoginList(login: snapshot.data)
                  : Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }
}

class Login {
  String email;
  String username;
  String password;

  Login({this.email, this.username, this.password});

  Login.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}

List<Login> parsePhotos(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Login>((json) => Login.fromJson(json)).toList();
}

Future<List<Login>> fetchPhotos(http.Client client) async {
  final response = await client.get('http://localhost:8081/send');

  return parsePhotos(response.body);
}

class LoginList extends StatelessWidget {
  final List<Login> login;

  LoginList({Key key, this.login}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(login[0].email);
  }
}
